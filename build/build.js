console.log('build.js')
const fs = require('fs')
const rm = require('rimraf')
const path = require('path')
const chalk = require('chalk')
const webpack = require('webpack')
const config = require('./config')
const webpackConfig = require('./webpack.prod.conf')
const t0 = process.hrtime()

console.log(chalk.gray('--------------------', config.actual().assetsRoot, '---------------------'))

rm(path.join(config.actual().assetsRoot, config.actual().assetsSubDirectory), err => {
  if (err) throw err
  webpack(webpackConfig, function(err, stats) {
    if (err) throw err
    process.stdout.write(
      stats.toString({
        colors: true,
        modules: false,
        children: false,
        chunks: false,
        chunkModules: false,
      }) + '\n'
    )

    if (stats.hasErrors()) {
      console.log(chalk.red('  Error.'))
      process.exit(1)
    }
    const t1 = process.hrtime(t0)
    console.log(
      chalk.cyan('  Completado en ' + t1[0] + '.' + t1[1] + ' segundos.')
    )
    console.log(chalk.cyan('  A las: ' + new Date()))
    console.log(chalk.grey(config.actual().assetsRoot, config.actual().assetsSubDirectory))
    // Asistencia
    if (process.env.NODE_ENV === 'development') {
      fs.appendFile('./.asistencia', new Date() + '\n', error => {
        if (error) {
          return console.log(error)
        }
        console.log('Asistencia guardada.')
      })
    }
  })
})
