export default class Asistencia {
  constructor(obj) {
    this.fecha = null;
    this.persona_id = null;
    this.unidad_productiva_id = null;
    this.centro_costo_id = null;
    this.tipo_hora_id = null;
    this.hora_entrada = null;
    this.hora_salida = null;
    this.minuto_entrada = null;
    this.minuto_salida = null;
    this.pad = (numero) => {
      let cadena = '' + numero;
      return cadena.length === 1 ? '0' + cadena : cadena;
    };

    Object.assign(this, obj)
  }

  get entrada() {
    return {
      hora: this.hora_entrada,
      minuto: this.minuto_entrada,
    }
  }

  set entrada(obj) {
    this.hora_entrada = obj.hora;
    this.minuto_entrada = obj.minuto;
  }

  get salida() {
    return {
      hora: this.hora_salida,
      minuto: this.minuto_salida,
    }
  }
  set salida(obj) {
    this.hora_salida = obj.hora;
    this.minuto_salida = obj.minuto;
  }

  get es_entrada_valida() {
    return !!this.hora_entrada && !!this.minuto_entrada //TODO: Validaciones centro_csto y tipo_hora
  }

  get es_salida_valida() {
    return !!this.hora_salida && !!this.minuto_salida && this.entrada_formato.localeCompare(this.salida_formato) == -1 //Existe y es entrada es menor a salida. Solo funciona en formato 24h
  }

  get entrada_formato() {
    return this.pad(this.hora_entrada) + ':' + this.pad(this.minuto_entrada);
  }
  get salida_formato() {
    return this.pad(this.hora_salida) + ':' + this.pad(this.minuto_salida);
  }
  get presentacion() {
    return this.fecha + ', ' + this.entrada_formato + this.salida_formato.length > 2 ? this.salida_formato : '.';
  }
}