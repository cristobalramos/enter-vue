import Partida from '@/modelos/Partida.js'
import Pago from '@/modelos/Pago.js'
import Cliente from '@/modelos/Cliente.js'

export default function() {
  this.id = null
  this.centro_costo_id = null
  this.centro_costo = null
  this.cliente_id = null
  this.cliente = new Cliente()
  this.codigo = null
  this.codigo_modificacion = null
  this.comentarios = null
  this.cotizacion_id = null
  this.unidad_productiva_id = null
  this.estado_cotizacion_id = null
  this.estado_cotizacion = null
  this.fecha_envio = null
  this.fecha_solicitud = null
  this.monto_afecto = null
  this.monto_exento = null
  this.monto_descuento = null
  this.monto_iva = null
  this.monto_neto = null
  this.monto_total = null
  // this.monto_descuento = this.montoDescuentoCotizacion()
  // this.monto_iva = this.montoIVACotizacion()
  // this.monto_neto = this.montoNetoCotizacion()
  // this.monto_total = this.montoTotalCotizacion()
  this.monto_utilidad = null
  this.nombre_presentacion = null
  this.porcentaje_descuento = null
  this.porcentaje_utilidad = null
  this.responsable_cotizacion_id = null
  this.responsable_cotizacion = {}
  this.responsable_proyecto_id = null
  this.responsable_proyecto = {}
  this.tipo_cotizacion_id = null
  this.tipo_cotizacion = {}
  this.tipo_documento_id = null
  this.tipo_documento = {}
  this.validez = null
  this.valor_cip = null
  this.valor_hora = null
  this.valor_uf = null
  this.created_at = null
  this.updated_at = null
  
  this.partidas = [new Partida()]
  this.pagos = [new Pago()]

  // this.montoNetoPresupuesto = function() {
  //   let valor = Object.values(this.partidas).reduce(
  //     (suma, linea) => {
  //       return suma + parseInt(linea.netoPresupuesto(), 10) || 0
  //     },
  //     0
  //   )
  //   return valor
  // },
  // this.montoUtilidadPresupuesto = function() {
  //   return (
  //     this.montoNetoPresupuesto() * this.porcentaje_utilidad / 100
  //   )
  // },
  // this.montoTotalPresupuesto = function() {
  //   return this.montoNetoPresupuesto() + this.montoUtilidadPresupuesto()
  // },
  // this.montoNetoCotizacion = function() {
  //   return this.montoTotalPresupuesto()
  // },

  // /**
  //  * El monto de descuento se calcula sobre el neto, ignorando la utilidad.
  //  */
  // this.montoDescuentoCotizacion = function() {
  //   let descuento =
  //     this.montoNetoCotizacion() * this.porcentaje_descuento / 100
  //   return parseInt(descuento, 10) || 0
  // },
  // this.montoIVACotizacion = function() {
  //   return this.iva * parseInt(this.montoNetoCotizacion(), 10) || 0
  // },
  // this.montoTotalCotizacion = function() {
  //   let iva = this.afectoAIVA ? this.montoIVACotizacion() : 0
  //   return iva + this.montoNetoCotizacion() - this.montoDescuentoCotizacion()
  // },

  this.nuevaPartida = ()=> {
    this.partidas.push(new Partida())
  },
  this.nuevoPago = ()=> {
    this.pagos.push(new Pago())
  }
}
