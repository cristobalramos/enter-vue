export default function() {
  this.proveedor = null
  this.contacto = null
  this.productos = [
    {
      codigo: null,
      nombre: null,
      cantidad: null,
      unidad: null,
      precio: null,
      impuestos: null,
      centro_costo_id: null,
    },
  ]
  this.pagos = [{
    fecha: null,
    monto: null,
    glosa: null,
  }]
  this.comentarios = null
  // referenciado_a
  // referenciado_por
  // tipo_factura
}
