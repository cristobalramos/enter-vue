import Vue from 'vue'
import Router from 'vue-router'
import routes from './routes'
import store from '@/store'

Vue.use(Router)

const esRutaDeIngreso = route => route.name.indexOf('ingreso') === 0
const estaAutenticado = () => store.getters.estaAutenticado

const router = new Router({
  mode: 'history',
  routes,
  transitionOnLoad: true,
})

router.beforeEach((to, from, next) => {
  store.dispatch('cargando')
  if (!esRutaDeIngreso(to) && !estaAutenticado()) {
    //No es ruta de ingreso y no está autenticado (no autorizado)
    next({ replace: true, name: 'ingreso' })
  } else if (esRutaDeIngreso(to) && estaAutenticado()) {
    // Está autenticado y quiere ver el ingreso (debe rechazar)
    next({ replace: true, name: 'escritorio' })
  }
  //No es ruta de ingreso y está autenticado, o no está autenticado pero es ruta de ingreso (autorizado, debe pasar)
  next()
})

router.afterEach((to, from) => {
  store.dispatch('terminarCarga')
  store.dispatch('cambiarTitulo', to.meta.title || 'Sin título')
})

export default router
