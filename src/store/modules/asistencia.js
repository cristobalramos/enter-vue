const state = {
  paso: null,
  asistencias: [],
}

const mutations = {
  CAMBIAR_PASO(state, paso) {
    state.paso = paso
  },
  PROXIMO_PASO(state) {
    ++state.paso
  },
  PASO_PREVIO(state) {
    --state.paso
  },
  AGREGAR_ASISTENCIA(state, asistencia) {
    if ('id' in asistencia === false ||
      !state.asistencias.find((a) => {
        return 'id' in a.id && asistencia.id === a.id
      })) {
      state.asistencias.push({
        persona: asistencia.persona,
        fecha: asistencia.fecha,
        entrada: {
          hora: asistencia.entrada.hora,
          minuto: asistencia.entrada.minuto,
        },
        salida: {
          hora: asistencia.salida.hora,
          minuto: asistencia.salida.minuto,
        },
        centro_costo: asistencia.centro_costo,
        tipo_hora: asistencia.tipo_hora,
      })
    }
  }
}

const actions = {
  borrarAsistencia(context) {
    context.state.asistencias = []
  },
}

const getters = {
  ultimaAsistenciaLocal: (state) => {
    return state.asistencias.length ? state.asistencias[state.asistencias.length - 1] : null
  },
}

export default {
  state,
  mutations,
  actions,
  getters,
}
