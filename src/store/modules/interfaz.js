const state = {
  cargando: false,
  nombre_software: 'SGC',
  titulo: 'Inicio',
  color: 'teal',
  color_texto: 'white',
  ver_menu_lateral: true,
  ver_unidades_productivas: false,
  ver_formulario_ingreso: true,
  ver_alerta: false,
  errores: null,
}

const mutations = {
  AGREGAR_ERROR(state, error) {
    state.errores = state.errores || []
    state.errores.push(error)
  },
  BORRAR_ERRORES(state) {
    state.errores = []
  },
  CAMBIAR_CARGANDO(state, cargando) {
    state.cargando = cargando
  },
  CAMBIAR_COLOR(state, color) {
    state.color = color
  },
  CAMBIAR_TITULO(state, titulo) {
    state.titulo = titulo
  },
  CAMBIAR_VER_MENU_LATERAL(state, valor) {
    state.ver_menu_lateral = valor
  },
  ALTERNAR_VER_MENU_LATERAL(state) {
    state.ver_menu_lateral = !state.ver_menu_lateral
  },
  ALTERNAR_VER_ALERTA(state) {
    state.ver_alerta = !state.ver_alerta
  },
  CAMBIAR_VER_FORMULARIO_INGRESO(state, valor) {
    state.ver_unidades_productivas = valor
  },
  CAMBIAR_VER_ALERTA(state, valor) {
    state.ver_alerta = valor
  },
}

const actions = {
  agregarError(context, { titulo, error, mensaje_usuario }) {
    console.error(titulo, error)
    context.commit('AGREGAR_ERROR', { titulo, error, mensaje_usuario })
  },
  borrarErrores(context) {
    context.commit('AGREGAR_ERROR')
  },
  cargando(context) {
    context.commit('CAMBIAR_CARGANDO', true)
    context.dispatch('cambiarTitulo', 'Cargando')
  },
  terminarCarga(context) {
    context.commit('CAMBIAR_CARGANDO', false)
  },
  cambiarColor(context, color) {
    context.commit('CAMBIAR_COLOR', color)
  },
  cambiarTitulo(context, titulo) {
    context.commit('CAMBIAR_TITULO', titulo)
    document.title = context.getters.tituloPagina
  },
  cambiarVerMenuLateral(context, valor) {
    context.commit('CAMBIAR_VER_MENU_LATERAL', valor)
  },
  cambiarVerFormularioIngreso(context, valor) {
    context.commit('CAMBIAR_VER_FORMULARIO_INGRESO', valor)
  },
  cambiarVerAlerta(context, valor) {
    context.commit('CAMBIAR_VER_ALERTA', valor)
  },
  alternarVerMenuLateral(context) {
    context.commit('ALTERNAR_VER_MENU_LATERAL')
  },
  alternarVerAlerta(context) {
    context.commit('ALTERNAR_VER_ALERTA')
  },
}

const getters = {
  tituloPagina: (state, getters) => {
    let titulo = state.titulo

    if (getters.nombreUnidadProductiva)
      titulo += ' - ' + getters.nombreUnidadProductiva
    else titulo += ' - ' + state.nombre_software

    return titulo
  },
  estaCargando: state => state.cargando,
  estiloBotonPrimario: state =>
    state.color + ' ' + state.color_texto,
  estiloBotonSecundario: state =>
    state.color + '--text text--lighten-2',
  verMenuLateral: state => state.ver_menu_lateral,
  verFormularioIngreso: state => state.ver_formulario_ingreso,
  ultimoError: state => state.errores ? state.errores[state.errores.length - 1] : null
}

export default {
  state,
  actions,
  mutations,
  getters,
}
