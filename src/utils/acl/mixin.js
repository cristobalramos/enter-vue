// @ts-check
import Vue from 'vue'
import Vuex from 'vuex'
import VueRouter from 'vue-router'

import { testPermission } from './checker'

/** @type {Array} */
let currentGlobal = []
let not = false

/**
 * Register all plugin actions
 *
 * @param {string|Array} initial initial permission
 * @param {boolean} acceptLocalRules if true accept local rules
 * @param {Object} globalRules definition of global rules
 * @param {VueRouter} router router object
 * @param {Vuex} store vuex store object
 * @param {string} notfound path for error on forbidden route
 */
export const register = (
  initial,
  acceptLocalRules,
  globalRules,
  router,
  store,
  notfound
) => {
  currentGlobal = Array.isArray(initial) ? initial : [initial]

  if (router !== null) {
    router.beforeEach((to, from, next) => {
      if (store && store.getters.aclPermission)
        currentGlobal = store.getters.aclPermission
        
      if (to.path === notfound) return next()

      /** @type {Array} */
      if (!('rule' in to.meta)) {
        return console.error(`[vue-acl] ${to.path} not have rule`)
      }
      let routePermission = to.meta.rule

      if (routePermission in globalRules) {
        routePermission = globalRules[routePermission]
      }

      if (!testPermission(currentGlobal, routePermission)) return next(notfound)
      return next()
    })
  }

  return {
    /**
     * Called before create component
     */
    beforeCreate() {
      const self = this

      this.$acl = {
        /**
         * Change current language
         * @param {string|Array} param
         */
        change(param) {
          param = Array.isArray(param) ? param : [param]

          if (currentGlobal.toString() !== param.toString()) {
            this.$emit('acl-permission-changed', param)
          }
        },

        /**
         * get current permission
         */
        get get() {
          return currentGlobal
        },

        /**
         * reverse current acl check
         */
        get not() {
          not = true
          return this
        },

        /**
         * Check if rule is valid currently
         * @param {string} ruleName rule name
         */
        check(ruleName) {
          const hasNot = not
          not = false

          if (ruleName in globalRules) {
            const result = testPermission(this.get, globalRules[ruleName])
            return hasNot ? !result : result
          }

          if (ruleName in self) {
            if (!acceptLocalRules) {
              return console.error('[vue-acl] acceptLocalRules is not enabled')
            }

            const result = testPermission(this.get, self[ruleName])
            return hasNot ? !result : result
          }

          return false
        },
      }

      this.$on('acl-permission-changed', newPermission => {
        currentGlobal = newPermission
        store.dispatch('aclChangePermission', newPermission)
        this.$forceUpdate()
      })
    },
  }
}
