import Vue from 'vue'
import Vuetify from 'vuetify'
import ListaCotizaciones from '@/components/Cotizaciones/ListaCotizaciones'
import CrearCotizacion from '@/components/Cotizaciones/CrearCotizacion'

Vue.use(Vuetify)

describe('Cotizaciones', () => {
  it('debe listar las cotizaciones', () => {
    const Constructor = Vue.extend(ListaCotizaciones)
    const vm = new Constructor().$mount()
    expect(vm.$el.querySelector('.datatable.table').textContent)
      .to.equal('Welcome to Your Vue.js App')
  })
  it('debe mostrar el formulario para crear cotizaciones', () => {
    const Constructor = Vue.extend(CrearCotizacion)
    const vm = new Constructor().$mount()
    expect(vm.$el.querySelector('.datatable.table').textContent)
      .to.equal('Welcome to Your Vue.js App')
  })
})